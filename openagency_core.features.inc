<?php
/**
 * @file
 * openagency_core.features.inc
 */

/**
 * Implements hook_views_api().
 */
function openagency_core_views_api() {
  return array("version" => "3.0");
}

/**
 * Implements hook_eck_bundle_info().
 */
function openagency_core_eck_bundle_info() {
  $items = array(
  'openagency_followup_openagency_followup' => array(
  'machine_name' => 'openagency_followup_openagency_followup',
  'entity_type' => 'openagency_followup',
  'name' => 'openagency_followup',
  'label' => 'Followup',
),
  'openagency_servicerequest_openagency_servicerequest' => array(
  'machine_name' => 'openagency_servicerequest_openagency_servicerequest',
  'entity_type' => 'openagency_servicerequest',
  'name' => 'openagency_servicerequest',
  'label' => 'Service Request',
),
  );
  return $items;
}

/**
 * Implements hook_eck_entity_type_info().
 */
function openagency_core_eck_entity_type_info() {
$items = array(
       'openagency_followup' => array(
  'name' => 'openagency_followup',
  'label' => 'Followup',
  'properties' => array(
    'title' => array(
      'label' => 'Title',
      'type' => 'text',
      'behavior' => 'title',
    ),
    'uid' => array(
      'label' => 'Author',
      'type' => 'integer',
      'behavior' => 'author',
    ),
    'created' => array(
      'label' => 'Created',
      'type' => 'integer',
      'behavior' => 'created',
    ),
    'changed' => array(
      'label' => 'Changed',
      'type' => 'integer',
      'behavior' => 'changed',
    ),
    'language' => array(
      'label' => 'Entity language',
      'type' => 'language',
      'behavior' => 'language',
    ),
  ),
),
       'openagency_servicerequest' => array(
  'name' => 'openagency_servicerequest',
  'label' => 'Service Request',
  'properties' => array(
    'title' => array(
      'label' => 'Title',
      'type' => 'text',
      'behavior' => 'title',
    ),
    'uid' => array(
      'label' => 'Author',
      'type' => 'integer',
      'behavior' => 'author',
    ),
    'created' => array(
      'label' => 'Created',
      'type' => 'integer',
      'behavior' => 'created',
    ),
    'changed' => array(
      'label' => 'Changed',
      'type' => 'integer',
      'behavior' => 'changed',
    ),
    'language' => array(
      'label' => 'Entity language',
      'type' => 'language',
      'behavior' => 'language',
    ),
  ),
),
  );
  return $items;
}
