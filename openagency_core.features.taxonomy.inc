<?php
/**
 * @file
 * openagency_core.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function openagency_core_taxonomy_default_vocabularies() {
  return array(
    'openagency_service_request_status' => array(
      'name' => 'Service Request Status',
      'machine_name' => 'openagency_service_request_status',
      'description' => '',
      'hierarchy' => '0',
      'module' => 'taxonomy',
      'weight' => '0',
    ),
    'openagency_status_filters' => array(
      'name' => 'Status Filters',
      'machine_name' => 'openagency_status_filters',
      'description' => '',
      'hierarchy' => '0',
      'module' => 'taxonomy',
      'weight' => '0',
    ),
  );
}
