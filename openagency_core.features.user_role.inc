<?php
/**
 * @file
 * openagency_core.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function openagency_core_user_default_roles() {
  $roles = array();

  // Exported role: OpenAgency Management.
  $roles['OpenAgency Management'] = array(
    'name' => 'OpenAgency Management',
    'weight' => '3',
  );

  // Exported role: OpenAgency Staff.
  $roles['OpenAgency Staff'] = array(
    'name' => 'OpenAgency Staff',
    'weight' => '2',
  );

  return $roles;
}
