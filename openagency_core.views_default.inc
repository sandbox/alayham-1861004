<?php
/**
 * @file
 * openagency_core.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function openagency_core_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'openagency_all_service_requests';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'eck_openagency_servicerequest';
  $view->human_name = 'All Service Requests';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Service Requests Assigned to Me';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'role';
  $handler->display->display_options['access']['role'] = array(
    4 => '4',
    3 => '3',
  );
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['distinct'] = TRUE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'counter' => 'counter',
    'type' => 'type',
    'field_openagency_request_status' => 'field_openagency_request_status',
    'title' => 'title',
    'name' => 'name',
    'created' => 'created',
    'changed' => 'changed',
    'view_link' => 'view_link',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'counter' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'type' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_openagency_request_status' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'title' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'name' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'created' => array(
      'sortable' => 1,
      'default_sort_order' => 'desc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'changed' => array(
      'sortable' => 1,
      'default_sort_order' => 'desc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'view_link' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  /* No results behavior: Global: Text area */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['empty'] = TRUE;
  $handler->display->display_options['empty']['area']['content'] = 'No service requests found';
  $handler->display->display_options['empty']['area']['format'] = 'plain_text';
  /* Relationship: Service Request: Author */
  $handler->display->display_options['relationships']['uid']['id'] = 'uid';
  $handler->display->display_options['relationships']['uid']['table'] = 'eck_openagency_servicerequest';
  $handler->display->display_options['relationships']['uid']['field'] = 'uid';
  /* Relationship: Service Request: Status (field_openagency_request_status) */
  $handler->display->display_options['relationships']['field_openagency_request_status_tid']['id'] = 'field_openagency_request_status_tid';
  $handler->display->display_options['relationships']['field_openagency_request_status_tid']['table'] = 'field_data_field_openagency_request_status';
  $handler->display->display_options['relationships']['field_openagency_request_status_tid']['field'] = 'field_openagency_request_status_tid';
  $handler->display->display_options['relationships']['field_openagency_request_status_tid']['label'] = 'Status';
  /* Relationship: Taxonomy term: Filters (field_filters) */
  $handler->display->display_options['relationships']['field_filters_tid']['id'] = 'field_filters_tid';
  $handler->display->display_options['relationships']['field_filters_tid']['table'] = 'field_data_field_filters';
  $handler->display->display_options['relationships']['field_filters_tid']['field'] = 'field_filters_tid';
  $handler->display->display_options['relationships']['field_filters_tid']['relationship'] = 'field_openagency_request_status_tid';
  $handler->display->display_options['relationships']['field_filters_tid']['label'] = 'Filter';
  /* Field: Global: View result counter */
  $handler->display->display_options['fields']['counter']['id'] = 'counter';
  $handler->display->display_options['fields']['counter']['table'] = 'views';
  $handler->display->display_options['fields']['counter']['field'] = 'counter';
  $handler->display->display_options['fields']['counter']['label'] = '#';
  $handler->display->display_options['fields']['counter']['counter_start'] = '1';
  /* Field: Service Request: openagency_servicerequest type */
  $handler->display->display_options['fields']['type']['id'] = 'type';
  $handler->display->display_options['fields']['type']['table'] = 'eck_openagency_servicerequest';
  $handler->display->display_options['fields']['type']['field'] = 'type';
  $handler->display->display_options['fields']['type']['label'] = 'Type';
  /* Field: Service Request: Status */
  $handler->display->display_options['fields']['field_openagency_request_status']['id'] = 'field_openagency_request_status';
  $handler->display->display_options['fields']['field_openagency_request_status']['table'] = 'field_data_field_openagency_request_status';
  $handler->display->display_options['fields']['field_openagency_request_status']['field'] = 'field_openagency_request_status';
  $handler->display->display_options['fields']['field_openagency_request_status']['type'] = 'taxonomy_term_reference_plain';
  /* Field: Service Request: Assigned To */
  $handler->display->display_options['fields']['field_openagency_assigned_to']['id'] = 'field_openagency_assigned_to';
  $handler->display->display_options['fields']['field_openagency_assigned_to']['table'] = 'field_data_field_openagency_assigned_to';
  $handler->display->display_options['fields']['field_openagency_assigned_to']['field'] = 'field_openagency_assigned_to';
  $handler->display->display_options['fields']['field_openagency_assigned_to']['settings'] = array(
    'link' => 0,
  );
  $handler->display->display_options['fields']['field_openagency_assigned_to']['delta_offset'] = '0';
  /* Field: Service Request: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'eck_openagency_servicerequest';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  /* Field: User: Name */
  $handler->display->display_options['fields']['name']['id'] = 'name';
  $handler->display->display_options['fields']['name']['table'] = 'users';
  $handler->display->display_options['fields']['name']['field'] = 'name';
  $handler->display->display_options['fields']['name']['relationship'] = 'uid';
  $handler->display->display_options['fields']['name']['label'] = 'Author';
  $handler->display->display_options['fields']['name']['link_to_user'] = FALSE;
  /* Field: Service Request: Created */
  $handler->display->display_options['fields']['created']['id'] = 'created';
  $handler->display->display_options['fields']['created']['table'] = 'eck_openagency_servicerequest';
  $handler->display->display_options['fields']['created']['field'] = 'created';
  $handler->display->display_options['fields']['created']['date_format'] = 'raw time ago';
  $handler->display->display_options['fields']['created']['custom_date_format'] = '2';
  /* Field: Service Request: Changed */
  $handler->display->display_options['fields']['changed']['id'] = 'changed';
  $handler->display->display_options['fields']['changed']['table'] = 'eck_openagency_servicerequest';
  $handler->display->display_options['fields']['changed']['field'] = 'changed';
  $handler->display->display_options['fields']['changed']['date_format'] = 'raw time ago';
  $handler->display->display_options['fields']['changed']['custom_date_format'] = '2';
  /* Field: Service Request: Link */
  $handler->display->display_options['fields']['view_link']['id'] = 'view_link';
  $handler->display->display_options['fields']['view_link']['table'] = 'eck_openagency_servicerequest';
  $handler->display->display_options['fields']['view_link']['field'] = 'view_link';
  /* Filter criterion: Service Request: openagency_servicerequest type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'eck_openagency_servicerequest';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['exposed'] = TRUE;
  $handler->display->display_options['filters']['type']['expose']['operator_id'] = 'type_op';
  $handler->display->display_options['filters']['type']['expose']['label'] = 'Type';
  $handler->display->display_options['filters']['type']['expose']['operator'] = 'type_op';
  $handler->display->display_options['filters']['type']['expose']['identifier'] = 'type';
  $handler->display->display_options['filters']['type']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
  );
  /* Filter criterion: Service Request: Status (field_openagency_request_status) */
  $handler->display->display_options['filters']['field_openagency_request_status_tid']['id'] = 'field_openagency_request_status_tid';
  $handler->display->display_options['filters']['field_openagency_request_status_tid']['table'] = 'field_data_field_openagency_request_status';
  $handler->display->display_options['filters']['field_openagency_request_status_tid']['field'] = 'field_openagency_request_status_tid';
  $handler->display->display_options['filters']['field_openagency_request_status_tid']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_openagency_request_status_tid']['expose']['operator_id'] = 'field_openagency_request_status_tid_op';
  $handler->display->display_options['filters']['field_openagency_request_status_tid']['expose']['label'] = 'Status';
  $handler->display->display_options['filters']['field_openagency_request_status_tid']['expose']['operator'] = 'field_openagency_request_status_tid_op';
  $handler->display->display_options['filters']['field_openagency_request_status_tid']['expose']['identifier'] = 'field_openagency_request_status_tid';
  $handler->display->display_options['filters']['field_openagency_request_status_tid']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
  );
  $handler->display->display_options['filters']['field_openagency_request_status_tid']['type'] = 'select';
  $handler->display->display_options['filters']['field_openagency_request_status_tid']['vocabulary'] = 'openagency_service_request_status';
  $handler->display->display_options['filters']['field_openagency_request_status_tid']['hierarchy'] = 1;
  /* Filter criterion: Taxonomy term: Filters (field_filters) */
  $handler->display->display_options['filters']['field_filters_tid']['id'] = 'field_filters_tid';
  $handler->display->display_options['filters']['field_filters_tid']['table'] = 'field_data_field_filters';
  $handler->display->display_options['filters']['field_filters_tid']['field'] = 'field_filters_tid';
  $handler->display->display_options['filters']['field_filters_tid']['relationship'] = 'field_openagency_request_status_tid';
  $handler->display->display_options['filters']['field_filters_tid']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_filters_tid']['expose']['operator_id'] = 'field_filters_tid_op';
  $handler->display->display_options['filters']['field_filters_tid']['expose']['label'] = 'Filters';
  $handler->display->display_options['filters']['field_filters_tid']['expose']['operator'] = 'field_filters_tid_op';
  $handler->display->display_options['filters']['field_filters_tid']['expose']['identifier'] = 'field_filters_tid';
  $handler->display->display_options['filters']['field_filters_tid']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
  );
  $handler->display->display_options['filters']['field_filters_tid']['type'] = 'select';
  $handler->display->display_options['filters']['field_filters_tid']['vocabulary'] = 'openagency_status_filters';
  $handler->display->display_options['filters']['field_filters_tid']['hierarchy'] = 1;

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page_1');
  $handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
  $handler->display->display_options['path'] = 'requests';
  $handler->display->display_options['menu']['type'] = 'normal';
  $handler->display->display_options['menu']['title'] = 'All Service Requsts';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['name'] = 'main-menu';
  $handler->display->display_options['menu']['context'] = 0;
  $export['openagency_all_service_requests'] = $view;

  $view = new view();
  $view->name = 'openagency_assigned_to_selection';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'users';
  $view->human_name = 'Assigned to Selection';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['access']['perm'] = 'access user profiles';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: User: Name */
  $handler->display->display_options['fields']['name']['id'] = 'name';
  $handler->display->display_options['fields']['name']['table'] = 'users';
  $handler->display->display_options['fields']['name']['field'] = 'name';
  $handler->display->display_options['fields']['name']['label'] = '';
  $handler->display->display_options['fields']['name']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['name']['alter']['ellipsis'] = FALSE;
  /* Filter criterion: User: Active */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'users';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = '1';
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: User: Roles */
  $handler->display->display_options['filters']['rid']['id'] = 'rid';
  $handler->display->display_options['filters']['rid']['table'] = 'users_roles';
  $handler->display->display_options['filters']['rid']['field'] = 'rid';
  $handler->display->display_options['filters']['rid']['value'] = array(
    3 => '3',
  );

  /* Display: Entity Reference */
  $handler = $view->new_display('entityreference', 'Entity Reference', 'entityreference_1');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'entityreference_style';
  $handler->display->display_options['style_options']['search_fields'] = array(
    'name' => 'name',
  );
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'entityreference_fields';
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $export['openagency_assigned_to_selection'] = $view;

  $view = new view();
  $view->name = 'openagency_followup_list';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'eck_openagency_followup';
  $view->human_name = 'Openagency Followup List';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'entity';
  $handler->display->display_options['row_options']['view_mode'] = 'teaser';
  /* Header: Global: Text area */
  $handler->display->display_options['header']['area']['id'] = 'area';
  $handler->display->display_options['header']['area']['table'] = 'views';
  $handler->display->display_options['header']['area']['field'] = 'area';
  $handler->display->display_options['header']['area']['empty'] = TRUE;
  $handler->display->display_options['header']['area']['content'] = '<?php 
  $srq=\'!1\';
  if(is_numeric($srq)){
	  $entity_type_name = \'openagency_followup\';
	  $entity_type = entity_type_load($entity_type_name);
	  foreach (Bundle::loadByEntityType($entity_type) as $bundle) {
		$bundle_name = $bundle->name;
		$entity = entity_create($entity_type_name, array(\'type\' => $bundle->name)); 
		$entity->field_followup_of[\'und\'][0][\'target_id\'] = $srq;
		$frm[$bundle->name] = array(
			\'#type\'=>\'fieldset\',
			\'#title\'=> t(\'Create @type\',array(\'@type\'=>$bundle->label)),
			\'#collapsible\'=>TRUE,
			\'#collapsed\'=>TRUE,
			\'form\' => drupal_get_form("eck__entity__form_add_{$entity_type_name}_{$bundle_name}", $entity),
		);
		$frm[$bundle->name][\'form\'][\'field_followup_of\'][\'und\'][0][\'target_id\'][\'#access\']=false;
	  }
	  print render($frm); 
  }else{
	print "Service request undefined";
  }

?>';
  $handler->display->display_options['header']['area']['format'] = 'php_code';
  $handler->display->display_options['header']['area']['tokenize'] = TRUE;
  /* No results behavior: Global: Text area */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['empty'] = TRUE;
  $handler->display->display_options['empty']['area']['content'] = 'No Followups have been created yet';
  $handler->display->display_options['empty']['area']['format'] = 'plain_text';
  /* Field: Followup: Id */
  $handler->display->display_options['fields']['id']['id'] = 'id';
  $handler->display->display_options['fields']['id']['table'] = 'eck_openagency_followup';
  $handler->display->display_options['fields']['id']['field'] = 'id';
  /* Sort criterion: Followup: Created */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'eck_openagency_followup';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['exposed'] = TRUE;
  $handler->display->display_options['sorts']['created']['expose']['label'] = 'Created';
  /* Contextual filter: Followup: Followup Of (field_followup_of) */
  $handler->display->display_options['arguments']['field_followup_of_target_id']['id'] = 'field_followup_of_target_id';
  $handler->display->display_options['arguments']['field_followup_of_target_id']['table'] = 'field_data_field_followup_of';
  $handler->display->display_options['arguments']['field_followup_of_target_id']['field'] = 'field_followup_of_target_id';
  $handler->display->display_options['arguments']['field_followup_of_target_id']['default_action'] = 'empty';
  $handler->display->display_options['arguments']['field_followup_of_target_id']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['field_followup_of_target_id']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['field_followup_of_target_id']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['field_followup_of_target_id']['summary_options']['items_per_page'] = '25';
  /* Filter criterion: Followup: openagency_followup type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'eck_openagency_followup';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['exposed'] = TRUE;
  $handler->display->display_options['filters']['type']['expose']['operator_id'] = 'type_op';
  $handler->display->display_options['filters']['type']['expose']['label'] = 'Followup type';
  $handler->display->display_options['filters']['type']['expose']['operator'] = 'type_op';
  $handler->display->display_options['filters']['type']['expose']['identifier'] = 'type';
  $handler->display->display_options['filters']['type']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
  );

  /* Display: EVA Field */
  $handler = $view->new_display('entity_view', 'EVA Field', 'entity_view_1');
  $handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
  $handler->display->display_options['entity_type'] = 'openagency_servicerequest';
  $handler->display->display_options['bundles'] = array(
    0 => 'openagency_servicerequest',
  );
  $export['openagency_followup_list'] = $view;

  $view = new view();
  $view->name = 'openagency_my_serive_requests';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'eck_openagency_servicerequest';
  $view->human_name = 'My Serive Requests';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Service Requests Assigned to Me';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'role';
  $handler->display->display_options['access']['role'] = array(
    3 => '3',
  );
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['distinct'] = TRUE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'counter' => 'counter',
    'type' => 'type',
    'field_openagency_request_status' => 'field_openagency_request_status',
    'title' => 'title',
    'name' => 'name',
    'created' => 'created',
    'changed' => 'changed',
    'view_link' => 'view_link',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'counter' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'type' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_openagency_request_status' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'title' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'name' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'created' => array(
      'sortable' => 1,
      'default_sort_order' => 'desc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'changed' => array(
      'sortable' => 1,
      'default_sort_order' => 'desc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'view_link' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  /* No results behavior: Global: Text area */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['empty'] = TRUE;
  $handler->display->display_options['empty']['area']['content'] = 'No service requests found';
  $handler->display->display_options['empty']['area']['format'] = 'plain_text';
  /* Relationship: Service Request: Author */
  $handler->display->display_options['relationships']['uid']['id'] = 'uid';
  $handler->display->display_options['relationships']['uid']['table'] = 'eck_openagency_servicerequest';
  $handler->display->display_options['relationships']['uid']['field'] = 'uid';
  /* Relationship: Service Request: Status (field_openagency_request_status) */
  $handler->display->display_options['relationships']['field_openagency_request_status_tid']['id'] = 'field_openagency_request_status_tid';
  $handler->display->display_options['relationships']['field_openagency_request_status_tid']['table'] = 'field_data_field_openagency_request_status';
  $handler->display->display_options['relationships']['field_openagency_request_status_tid']['field'] = 'field_openagency_request_status_tid';
  $handler->display->display_options['relationships']['field_openagency_request_status_tid']['label'] = 'Status';
  /* Relationship: Taxonomy term: Filters (field_filters) */
  $handler->display->display_options['relationships']['field_filters_tid']['id'] = 'field_filters_tid';
  $handler->display->display_options['relationships']['field_filters_tid']['table'] = 'field_data_field_filters';
  $handler->display->display_options['relationships']['field_filters_tid']['field'] = 'field_filters_tid';
  $handler->display->display_options['relationships']['field_filters_tid']['relationship'] = 'field_openagency_request_status_tid';
  $handler->display->display_options['relationships']['field_filters_tid']['label'] = 'Filter';
  /* Field: Global: View result counter */
  $handler->display->display_options['fields']['counter']['id'] = 'counter';
  $handler->display->display_options['fields']['counter']['table'] = 'views';
  $handler->display->display_options['fields']['counter']['field'] = 'counter';
  $handler->display->display_options['fields']['counter']['label'] = '#';
  $handler->display->display_options['fields']['counter']['counter_start'] = '1';
  /* Field: Service Request: openagency_servicerequest type */
  $handler->display->display_options['fields']['type']['id'] = 'type';
  $handler->display->display_options['fields']['type']['table'] = 'eck_openagency_servicerequest';
  $handler->display->display_options['fields']['type']['field'] = 'type';
  $handler->display->display_options['fields']['type']['label'] = 'Type';
  /* Field: Service Request: Status */
  $handler->display->display_options['fields']['field_openagency_request_status']['id'] = 'field_openagency_request_status';
  $handler->display->display_options['fields']['field_openagency_request_status']['table'] = 'field_data_field_openagency_request_status';
  $handler->display->display_options['fields']['field_openagency_request_status']['field'] = 'field_openagency_request_status';
  $handler->display->display_options['fields']['field_openagency_request_status']['type'] = 'taxonomy_term_reference_plain';
  /* Field: Service Request: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'eck_openagency_servicerequest';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  /* Field: User: Name */
  $handler->display->display_options['fields']['name']['id'] = 'name';
  $handler->display->display_options['fields']['name']['table'] = 'users';
  $handler->display->display_options['fields']['name']['field'] = 'name';
  $handler->display->display_options['fields']['name']['relationship'] = 'uid';
  $handler->display->display_options['fields']['name']['label'] = 'Author';
  $handler->display->display_options['fields']['name']['link_to_user'] = FALSE;
  /* Field: Service Request: Created */
  $handler->display->display_options['fields']['created']['id'] = 'created';
  $handler->display->display_options['fields']['created']['table'] = 'eck_openagency_servicerequest';
  $handler->display->display_options['fields']['created']['field'] = 'created';
  $handler->display->display_options['fields']['created']['date_format'] = 'raw time ago';
  $handler->display->display_options['fields']['created']['custom_date_format'] = '2';
  /* Field: Service Request: Changed */
  $handler->display->display_options['fields']['changed']['id'] = 'changed';
  $handler->display->display_options['fields']['changed']['table'] = 'eck_openagency_servicerequest';
  $handler->display->display_options['fields']['changed']['field'] = 'changed';
  $handler->display->display_options['fields']['changed']['date_format'] = 'raw time ago';
  $handler->display->display_options['fields']['changed']['custom_date_format'] = '2';
  /* Field: Service Request: Link */
  $handler->display->display_options['fields']['view_link']['id'] = 'view_link';
  $handler->display->display_options['fields']['view_link']['table'] = 'eck_openagency_servicerequest';
  $handler->display->display_options['fields']['view_link']['field'] = 'view_link';
  /* Contextual filter: Service Request: Assigned To (field_openagency_assigned_to) */
  $handler->display->display_options['arguments']['field_openagency_assigned_to_target_id']['id'] = 'field_openagency_assigned_to_target_id';
  $handler->display->display_options['arguments']['field_openagency_assigned_to_target_id']['table'] = 'field_data_field_openagency_assigned_to';
  $handler->display->display_options['arguments']['field_openagency_assigned_to_target_id']['field'] = 'field_openagency_assigned_to_target_id';
  $handler->display->display_options['arguments']['field_openagency_assigned_to_target_id']['default_action'] = 'default';
  $handler->display->display_options['arguments']['field_openagency_assigned_to_target_id']['default_argument_type'] = 'current_user';
  $handler->display->display_options['arguments']['field_openagency_assigned_to_target_id']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['field_openagency_assigned_to_target_id']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['field_openagency_assigned_to_target_id']['summary_options']['items_per_page'] = '25';
  /* Filter criterion: Service Request: openagency_servicerequest type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'eck_openagency_servicerequest';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['exposed'] = TRUE;
  $handler->display->display_options['filters']['type']['expose']['operator_id'] = 'type_op';
  $handler->display->display_options['filters']['type']['expose']['label'] = 'Type';
  $handler->display->display_options['filters']['type']['expose']['operator'] = 'type_op';
  $handler->display->display_options['filters']['type']['expose']['identifier'] = 'type';
  $handler->display->display_options['filters']['type']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
  );
  /* Filter criterion: Service Request: Status (field_openagency_request_status) */
  $handler->display->display_options['filters']['field_openagency_request_status_tid']['id'] = 'field_openagency_request_status_tid';
  $handler->display->display_options['filters']['field_openagency_request_status_tid']['table'] = 'field_data_field_openagency_request_status';
  $handler->display->display_options['filters']['field_openagency_request_status_tid']['field'] = 'field_openagency_request_status_tid';
  $handler->display->display_options['filters']['field_openagency_request_status_tid']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_openagency_request_status_tid']['expose']['operator_id'] = 'field_openagency_request_status_tid_op';
  $handler->display->display_options['filters']['field_openagency_request_status_tid']['expose']['label'] = 'Status';
  $handler->display->display_options['filters']['field_openagency_request_status_tid']['expose']['operator'] = 'field_openagency_request_status_tid_op';
  $handler->display->display_options['filters']['field_openagency_request_status_tid']['expose']['identifier'] = 'field_openagency_request_status_tid';
  $handler->display->display_options['filters']['field_openagency_request_status_tid']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
  );
  $handler->display->display_options['filters']['field_openagency_request_status_tid']['type'] = 'select';
  $handler->display->display_options['filters']['field_openagency_request_status_tid']['vocabulary'] = 'openagency_service_request_status';
  $handler->display->display_options['filters']['field_openagency_request_status_tid']['hierarchy'] = 1;
  /* Filter criterion: Taxonomy term: Filters (field_filters) */
  $handler->display->display_options['filters']['field_filters_tid']['id'] = 'field_filters_tid';
  $handler->display->display_options['filters']['field_filters_tid']['table'] = 'field_data_field_filters';
  $handler->display->display_options['filters']['field_filters_tid']['field'] = 'field_filters_tid';
  $handler->display->display_options['filters']['field_filters_tid']['relationship'] = 'field_openagency_request_status_tid';
  $handler->display->display_options['filters']['field_filters_tid']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_filters_tid']['expose']['operator_id'] = 'field_filters_tid_op';
  $handler->display->display_options['filters']['field_filters_tid']['expose']['label'] = 'Filters';
  $handler->display->display_options['filters']['field_filters_tid']['expose']['operator'] = 'field_filters_tid_op';
  $handler->display->display_options['filters']['field_filters_tid']['expose']['identifier'] = 'field_filters_tid';
  $handler->display->display_options['filters']['field_filters_tid']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
  );
  $handler->display->display_options['filters']['field_filters_tid']['type'] = 'select';
  $handler->display->display_options['filters']['field_filters_tid']['vocabulary'] = 'openagency_status_filters';
  $handler->display->display_options['filters']['field_filters_tid']['hierarchy'] = 1;

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page_1');
  $handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
  $handler->display->display_options['path'] = 'myrequests';
  $handler->display->display_options['menu']['type'] = 'normal';
  $handler->display->display_options['menu']['title'] = 'My Service Requests';
  $handler->display->display_options['menu']['description'] = 'Service Requests Assigned to me';
  $handler->display->display_options['menu']['weight'] = '1';
  $handler->display->display_options['menu']['name'] = 'main-menu';
  $handler->display->display_options['menu']['context'] = 0;
  $export['openagency_my_serive_requests'] = $view;

  return $export;
}
